;;   ___ _ __  ___ _   _ _ __ ___
;;  / _ \ '_ \/ __| | | | '__/ _ \
;; |  __/ | | \__ \ |_| | | |  __/
;;  \___|_| |_|___/\__,_|_|  \___|
;;
;; A > R5RS testing framework
;; Allows unit and integration tests with mocking capabilities.
;; PUBLIC DOMAIN, refer to LICENSE-file.
;; TODO: Supports source code locations.
;;       read the file two times:
;;         - as datums, only top-level
;;         - line wise with count.
;;       Then compare the datum to the file-content ...

;; This file enables ANSI escape sequences @see `Select Graphic Rendition`
;; for using SGRs, I got help of ski and jcowan @libera.chat#scheme
(include "colors.scm")

(define ensure-asserts-within-test-case 0)
(define ensure-executed-asserts 0)
(define ensure-executed-tests 0)
(define ensure-suceeded-tests 0)
(define ensure-failed-tests 0)
(define ensure-skipped-tests 0)
(define test-group-name 'none)

(define ensure-beeing-verbose? #t)

(define test-begin!
  (lambda (name)
    (if (eq? test-group-name 'none)
        (display "# ")
        (display "## "))
    (set! test-group-name name)
    (display name) (newline)))

(eval
  (quote
    (define-syntax test
      (syntax-rules (expand-file asserting test expand-test intern evaluate expand-assert put-cursor increment-success! increment-fail!)
        ;; end of tests.
        ((_ put-cursor)
         (if ensure-beeing-verbose?
             ; After writing out the test case, we can prepend the result
             ; by going to the start of the line and overwrite "Test".
             ;; reasoning: elapsed time is unkown.
             (display (string #\escape #\[ #\2 #\K #\return))))
        ((_ increment-success!)
         (begin
           (test put-cursor)
           (set! ensure-suceeded-tests (+ ensure-suceeded-tests 1))
           (set! ensure-executed-asserts (+ ensure-executed-asserts ensure-asserts-within-test-case))
           (when ensure-beeing-verbose?
             (colored-display text-green " OK ")
             (newline))))
        ((_ increment-fail! number comparator? expected actual)
         (begin
           (test put-cursor)
           (set! ensure-failed-tests (+ ensure-failed-tests 1))
           (set! ensure-executed-asserts (+ ensure-executed-asserts number))
           (when ensure-beeing-verbose?
             (colored-display text-red "FAIL")
             (newline)
             (display "failed assert #") (display number)
             (display " with comparator: ") (display comparator?)
             (newline)
             (colored-display text-white "  expected: ") (pp expected)
             (colored-display text-red "  actual:   ") (pp actual))))
       ((_ expand-assert comparator? expected actual)
        (apply comparator? (list expected actual)))
       ((_ intern end-without-assert) end-without-assert)
       ((_ intern assert (comparator? expected actual))
        (begin
          (set! ensure-asserts-within-test-case (+ 1 ensure-asserts-within-test-case))
          ;(test evaluate (reverse (cons `(comparator? ,expected ,actual) evals)))
          (if (test expand-assert comparator? expected actual)
              (test increment-success!)
              (test increment-fail!
                          ensure-asserts-within-test-case
                          comparator? expected actual))))
       ;; assertion within test sub-macro found
       ((_ intern assert (comparator? expected actual) body ...)
        (begin
          (set! ensure-asserts-within-test-case (+ 1 ensure-asserts-within-test-case))
          (if (test expand-assert comparator? expected actual)
              (test intern body ...)
              (test increment-fail!
                          ensure-asserts-within-test-case
                          comparator? expected actual))))
       ((_ intern stmt body ...)
        (begin
          stmt
          (test intern body ...)))
       ;; starts actual test
       ((_ test-title body ...)
        (begin
          (set! ensure-executed-tests (+ 1 ensure-executed-tests))
          (set! ensure-asserts-within-test-case 0)
          (if ensure-beeing-verbose?
              (begin
                (display "Test #")
                (display ensure-executed-tests)
                (display " - ")
                (display test-title)))
          (test intern body ...)))
        ;; fail: empty test - redundant
        ((_ title)
         (let ((issue "no assertions used (bad usage)."))
           (if ensure-beeing-verbose?
               (begin
                 (colored-display text-yellow "SKIP")
                 (display " #") (display ensure-executed-tests)
                 (display " - ") (display issue) (newline))
               issue)))))))

;; TODO: `(test-against *.files)` should inject quoting to read-in-macros,
;;       before executing - IF this is possible. I guess it should be.
;(define-syntax trace-macro (syntax-rules () ((_ x) (quote x))))

;;; @deprecated
;;; enables to load a certain procedure from a loaded file.
;(define resolve
;  (lambda (file sym)
;    (let ((found (call-with-current-continuation (lambda (k) k))))
;      (letrec ((traverse
;                 (lambda (datum)
;                   (if (pair? datum)
;                       (if (eq? (car datum) sym)
;                           (found (cadr datum))
;                           (if (pair? (car datum))
;                               (traverse (car datum))
;                               (traverse (cdr datum))))))))
;        (if (procedure? found)
;            (begin
;              (for-each traverse bindings)
;              #f)
;            (eval found))))))

;; preprocesses all files and extracts
;;  - `(define (...) ...)`
;;  - `(define _ (lambda (...) ...))`
;;  TODO: - `(define-syntax _ ...)`
;;
;  Puts all resolved targets into a variable to refer to within tests.
(define test-against
  (lambda (file)
    (display "Load file: ") (display file) (newline)
    (call-with-input-file
      file
      (lambda (p)
        (let ((transform-to-lambda?
                (lambda (datum procs)
                  (if (eq? 'define (car datum))
                      ;(or (pair? (cadr datum)) ; syntax: (define (... => procedure
                      ;    (and ; syntax: (define _ (lambda ... => procedure
                      ;      (pair? (caddr datum))
                      ;      (eq? 'lambda (caaddr datum)))))
                      (cons
                        (if (pair? (cadr datum))
                            ; (define (name args) ... => name . (lambda (args) ...
                            (let ((proc `((lambda ,(cdadr datum) ,@(cddr datum)))))
                              (cons (caadr datum) proc))
                            ; (define _ (lambda ... => 'name . (lambda ...
                            (cons (cadr datum) (cddr datum)))
                        procs)
                      procs))))

          (if (char=? #\# (peek-char p))
              (read-line p))
              (let filter ((procs '()))
                (let ((datum (read p)))
                  (if (eof-object? datum)
                      (reverse procs)
                      (filter (transform-to-lambda? datum procs))))))))))

(define test-summary!
  (lambda ()
    (display "----------------------") (newline)
    (display " ")
    (cond
      ((= 0 ensure-executed-tests)
       (display "No tests executed.") (newline))
      ((= 0 ensure-failed-tests)
       (colored-display text-green (number->string ensure-suceeded-tests))
       (display " tests succeded.") (newline))
      (else
        (display ensure-executed-tests)
        (display " tests executed, but ")
        (colored-display text-red (number->string ensure-failed-tests))
        (display " failed.")
        (newline)))
    (display " ") (display ensure-executed-asserts)
    (display " asserts executed.") (newline)
    (exit ensure-failed-tests)))
