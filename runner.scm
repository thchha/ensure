#!/usr/bin/env gsi-script

(include "colors.scm")

(define (main . files)
  (let ((temporary-file (string-append "ENSURE-" (number->string (time->seconds (current-time)))))
        (test-summary!
          (lambda (executed asserts skipped succeeded failed)
            (display "----------------------") (newline)
            (display " ") (display asserts)
            (display " asserts executed.")
            (when (> skipped 0)
              (newline) (display " ") (colored-display text-yellow skipped)
              (display " tests skipped."))
            (newline) (display " ")
            (cond
              ((= 0 executed)
               (colored-display text-white "0")
               (display " tests executed.") (newline)
               (exit 1))
              ((= 0 failed)
               (colored-display text-green succeeded)
               (display " tests succeded.") (newline)
               (exit 0))
              (else
                (display executed)
                (display " tests executed, but ")
                (colored-display text-red failed)
                (display " failed.") (newline)
                (exit 2))))))

    (colored-display text-black "> https://gitlab.com/thchha/ensure") (newline)

    ;; init state
    (call-with-output-file
      temporary-file
      (lambda (p)
        (pp (list 0 0 0 0 0) p)))

    ;; for each argument, run in a dedicated process
    (for-each
      (lambda (test-file)
        (if (file-exists? test-file)
            (for-each
              display
              (call-with-input-process
                (list path: "gsi"
                      arguments: (list
                                   "lib/ensure/ensuring.scm"
                                   "-e"
                                   (string-append
                                   "(call-with-input-file \"" temporary-file "\"
                                     (lambda (in)
                                       (let ((content (read in)))
                                         (set! ensure-executed-tests (car content))
                                         (set! ensure-executed-asserts (cadr content))
                                         (set! ensure-skipped-tests (caddr content))
                                         (set! ensure-suceeded-tests (cadddr content))
                                         (set! ensure-failed-tests (car (cddddr content))) 'end)))")
                                   test-file
                                   "-e" ;; bump test
                                   (string-append
                                     "(let ((file \"" temporary-file "\"))
                                       (call-with-input-file
                                         file
                                         (lambda (in)
                                           (let ((content (read in)))
                                             (call-with-output-file
                                               file
                                               (lambda (p)
                                                 (pp (list
                                                       ensure-executed-tests
                                                       ensure-executed-asserts
                                                       ensure-skipped-tests
                                                       ensure-suceeded-tests
                                                       ensure-failed-tests)
                                                     p)))))))")))
                (lambda (p)
                  (read-all p (lambda (p) (read-line p #\newline #t))))))))
      files)

    (call-with-input-file
      temporary-file
      (lambda (in)
        (let ((content (read in)))
          (delete-file temporary-file)

          (test-summary!
            (car content)
            (cadr content)
            (caddr content)
            (cadddr content)
            (car (cddddr content))))))))
